options(bitmapType='cairo')
options(device='cairo')
options(spinner.color="#144273")
Sys.setlocale("LC_COLLATE","C")

library(httr)
library(DT)
library(Cairo)
library(plotly)
# library(ggplot2)
# 
library(shinyjs)
# library(grid)
# library(gridExtra)
library(DBI)
library(RPostgreSQL)
# 
library(pool)
# library(reshape2)
# library(RColorBrewer)
# library(scales)
# library(stringr)
# 
library(PKI)
# library(SixSigma)
library(shinycssloaders)
library(shinyBS)
library(shinydashboard)
# 
library(shiny)
library(jsonlite)
library(base64url)
library(shinyWidgets)
# 
library(future)
library(parallel)
library(promises)
library(data.table)
library(dplyr)
library(lubridate)
# library(collapsibleTree)
# 
# library(DiagrammeR)
# library(webshot)
# library(htmlwidgets)
# library(data.tree)

Cairo()
config = plotly::config

#voor PCF
port <- Sys.getenv('PORT') 
if (nchar(port)==0){
  port = "8081"
}
print(port)

# addResourcePath('www', paste0('www'))
# future:::ClusterRegistry("stop")
# plan(multiprocess, workers=2)
# setwd("c:/Jibes/RWS-risico/")
shiny::runApp('./applicatie',host = '0.0.0.0', port = as.numeric(port))

