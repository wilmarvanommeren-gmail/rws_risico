# install.packages(c(), Ncpus = 8, dependencies = T)
packages = c(
  "httr",
  "DT",
  "Cairo",
  "plotly",
  "shinyjs",
  "DBI",
  "RPostgreSQL",
  "pool",
  "PKI",
  "shinycssloaders",
  "shinyBS",
  "shinydashboard",
  "shiny",
  "jsonlite",
  "base64url",
  "shinyWidgets",
  "future",
  "parallel",
  "promises",
  "data.table",
  "dplyr",
  "lubridate"
)

install.packages(packages, Ncpus = 8, dependencies = T, clean = T)
