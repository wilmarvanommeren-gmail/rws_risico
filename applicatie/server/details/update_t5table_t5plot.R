update_t5table_t5plot <- function (query, omgeving, trimesterend_dates, height, limit,
                                   project,
                                   trimester,
                                   risktype,
                                   risktypekind, 
                                   riskstatusdescription, 
                                   allocationdescription,
                                   accountingmethod, 
                                   ownername){
  # save(query, omgeving, trimesterend_dates, height, limit,
  #      project,
  #      trimester,
  #      risktype,
  #      risktypekind,
  #      riskstatusdescription,
  #      allocationdescription,
  #      accountingmethod,
  #      ownername, file='test4.Rdata')
  # load('./applicatie/test4.Rdata')

  # print( r_values$query)
  # Get top 5 risks in selected trimester
  select_tr = select_latest_risk_with_rmnodeid(query, connect_to_dbase(omgeving), max_date=query$dateend, 
                                               limit=limit,
                                               risktypekind=risktypekind, 
                                               riskstatusdescription=riskstatusdescription, 
                                               allocationdescription=allocationdescription,
                                               accountingmethod=accountingmethod, 
                                               ownername=ownername)
  
  ## Create top x table
  top5_table <- NULL
  if (!is.null(query$select)){
    # Long Running Task
    if (nrow(select_tr)>0 & ncol(select_tr)>0){
      top5_table <- create_top5_trimester_table(query, connect_to_dbase(omgeving), select_tr, 
                                                risktypekind=risktypekind, 
                                                riskstatusdescription=riskstatusdescription, 
                                                allocationdescription=allocationdescription,
                                                accountingmethod=accountingmethod, 
                                                ownername=ownername)
    }
  }
  # format table names
  if (!is.null(top5_table)){
    risk_color_1 <- select_risk_color_with_riskid(connect_to_dbase(omgeving), top5_table$riskid, query, max_date=query$dateend)
    if (nrow(risk_color_1)>0){
      names(risk_color_1)[2]<-'risk_color_1'
      top5_table <- merge(top5_table, risk_color_1[,1:2], by= 'riskid', all.x=T)
    } else {
      top5_table$risk_color_1 = NA
    }
    risk_color_2 <- select_risk_color_with_riskid(connect_to_dbase(omgeving), top5_table$riskid, query, max_date=query$datestart)
    if (nrow(risk_color_2)>0){
      names(risk_color_2)[2]<-'risk_color_2'
      top5_table <- merge(top5_table, risk_color_2[,1:2], by= 'riskid', all.x=T)
    } else {
      top5_table$risk_color_2 <- NA}
    top5_table = top5_table[order(top5_table[,paste0(query$risctype,' ', format(as.Date(query$dateend), "%d-%m-%Y")), with=FALSE], decreasing = T),]
    formatted_names <- gsub('risknumber', 'No.', names(top5_table))
    formatted_names <- gsub(query$risctype, convert_from_dbaserisk(query$risctype), formatted_names)
    formatted_names <- gsub('\\brisk\\b', 'Risico', formatted_names)
    names(top5_table)<-formatted_names
    top5_table_unformatted <- top5_table
    top5_table <- datatable(top5_table[, !grepl('riskid', formatted_names), with=F], rownames = F, selection = 'single',  
                            options=list(dom='<"top">ft',pageLength = -1, language = list('search'="Zoek:"), 
                                         columnDefs = list(list(targets = (ncol(top5_table)-3):(ncol(top5_table)-2), visible = FALSE)), rowCallback = 
                                           JS("function(row, data, index) {var full_text = 'Klik op een rij voor aanvullende informatie';
                                              $(row).attr('title', full_text);
                                              $(row).css('cursor', 'pointer');}")
                            ))%>% 
      formatStyle(2,'risk_color_1', backgroundColor = styleEqual(top5_table$risk_color_1, paste0('#',top5_table$risk_color_1)))%>% 
      formatStyle(3,'risk_color_2', backgroundColor = styleEqual(top5_table$risk_color_2, paste0('#',top5_table$risk_color_2)))
  } else {
    top5_table_unformatted <- data.frame()
    top5_table <- empty_datatable
  }
  
  
  ## Create top x trendline plot
  top5_plot <- empty_plot(height*.4, 'No trend data found')
  if (!is.null(query$select) & nrow(select_tr)>0 & ncol(select_tr)>0){
    all_risks = select_risk_overtime_with_rmnodeid(query, connect_to_dbase(omgeving), max_date=query$dateend, riskids=select_tr$riskid, 
                                                   risktypekind=risktypekind, 
                                                   riskstatusdescription=riskstatusdescription, 
                                                   allocationdescription=allocationdescription,
                                                   accountingmethod=accountingmethod, 
                                                   ownername=ownername)
    trend_table <- reshape(all_risks[, !grepl('riskid', names(all_risks))], 
                           idvar = 'date', timevar='risknumber', direction='wide')
    top5_plot <- create_trendplot_top5(query, trend_table[order(as.Date(trend_table$date, format= "%d/%m/%Y")),],
                                       min(as.Date(trend_table$date)),max(as.Date(trend_table$date)), height=height)
  }

  return(list(top5_table, top5_plot, top5_table_unformatted))}