#setwd('./applicatie')

# Load datasets
load('./data/test_query.RData')
load('./data/test_query2.RData')
# Load functions and variables
source('./functions/dbase_functionality.R')
source('./functions/general.R')
source('./functions/SQL_query.R')
source('./functions/oauth.R')
source('./functions/plotly_functions.R')
source('./functions/adapted_fishbone.R')
source('./functions/df_functions.R')
source('./variables/styling.R')
source('./variables/query.R')
source('./functions/total_risk_calculator.R')
source('./server/details/update_t5table_t5plot.R')
source('./server/details/update_totalplot.R')
source('ui_home.R')

# Do magic
test_query = PKI.load.key(test_query)
test_query = as.data.frame(apply(query_details, 2, function(x) {
  strsplit(as.character(rawToChar(PKI.decrypt(x, test_query))), ',', fixed = T)}))
colnames(test_query) = unlist(lapply(test_query[1,], as.character))

# load total dataset
stored_totalrisk_test = list()
stored_totalrisk_acceptatie = list()
stored_totalrisk_productie = list()
stored_totalrisk_ontwikkel = list()

if (file.exists("./data/stored_totalrisk_test.Rdata")){
  load("./data/stored_totalrisk_test.Rdata")
  stored_totalrisk_test <- stored_totalrisk
  }
if (file.exists("./data/stored_totalrisk_acceptatie.Rdata")){
  load("./data/stored_totalrisk_acceptatie.Rdata")
  stored_totalrisk_acceptatie<- stored_totalrisk}
if (file.exists("./data/stored_totalrisk_productie.Rdata")){
  load("./data/stored_totalrisk_productie.Rdata")
  stored_totalrisk_productie<- stored_totalrisk}
if (file.exists("./data/stored_totalrisk_ontwikkel.Rdata")){
  load("./data/stored_totalrisk_ontwikkel.Rdata")
  stored_totalrisk_ontwikkel <- stored_totalrisk}

# Define list of selectinput ids
filter_ids <- list('_basetrimestersl_details', 
                   '_risktype_details', 
                   '_project_details_pick', 
                   '_risckind_details', 
                   '_riscstatus_details', 
                   '_alloc_details', 
                   '_beheer_details', 
                   '_account_details')

# Global functions
convert_to_dbaserisk <- function(risctype){
  if (risctype=='geld'){
    risctype = 'charge'
  } else if (risctype=='tijd'){
    risctype = 'time'
  } else {
    risctype = 'riskscore'
  }
  return(risctype)
}

convert_from_dbaserisk <- function(risctype){
  if (risctype=='charge'){
    risctype = 'Geld'
  } else if (risctype=='time'){
    risctype = 'Tijd'
  } else {
    risctype = 'Score'
  }
  return(risctype)
}

LoadToEnvironment <- function(RData, env=new.env()) {
  tryCatch(load(RData, env), error=function(e) print(e))
  return(env)
}
