

calculate_total_risk <- function(omgeving, rmnode, outputfile='./data/stored_totalrisk.Rdata', periode='month', max_runs=-1, max_time=-1){
  # save(omgeving, rmnode, outputfile, periode, file='totalrisk.Rdata')
  periode = tolower(periode)
  if (!(periode %in% c('day', 'month', 'week', 'year'))){
    warning(paste('Periode moet zijn: day, month, week or year. Gegegeven periode:',periode,'. Gezet naar standaard waarde: month'))
    periode = 'month'
  }
  
  run = 1
  db_conn = connect_to_dbase(omgeving)
  if(identical(db_conn, F)){
    print('calculate_total_risk: db_conn fail')
  } else {
    print('calculate_total_risk: db_conn success')
  }
  run_start = Sys.time()
  
  dummy_df <- data.frame(matrix(ncol=22, nrow=0))
  names(dummy_df) <- c('date','riskid','rmnodeid','charge','time','riskscore','chargeclass','timeclass','chanceclass','quant_date','risknumber','risk','risktypekind','riskstatusdescription','riskstatusid','allocationdescription',
                       'allocationid','accountingmethod','ownername','ownerid','risk_date','pijldatum')
  stored_totalrisk = dummy_df
  while (TRUE) {
    if (run-1 == max_runs | (Sys.time()-run_start>max_time) & max_time != -1){
      break
    }
    
    start = Sys.time() 
    # createfile
    
    gc()
    
    
    query_rm <- list()
    rmnodeid = rmnode$rmnodeid
    # if (rmnodeid %in% names(stored_totalrisk)){
    #   next()
    # }
    query_rm$select <- unique(rmnode$rmnodeid)
    min_date = select_from_quant_min_actual_date(query_rm, connect_to_dbase(omgeving))$min
    min_date = floor_date(min_date, 'month')%m+% months(1)
    query_rm$risctype <- 'charge'
    
    if (!is.na(min_date)){
      total_risk_dates = dummy_df
      all_dates <- seq(as.Date(min_date, tz=''), as.Date(floor_date(Sys.Date(), 'month')%m+% months(1), tz=''), by=periode)
      for (idx_d in 1:length(all_dates)){
        gc()
        start=Sys.time()
        print(paste('calculate_total_risk:',idx_d/length(all_dates)*100,'%'))
        try(file.remove(list.files('./data/', pattern=paste0('total_data_[[:digit:]]*.[[:digit:]]*_',names(omgeving),'.R'), full.names = T)))
        
        save(idx_d, file = paste0('./data/total_data_',round(idx_d/length(all_dates)*100, 2),'_',names(omgeving),'.R'))
        if(length(dbListConnections (PostgreSQL ()))==0 | !dbIsValid(db_conn) | !check_connection(db_conn, 'ipb_risk')){
          try(disconnect_all(db_conn))
          db_conn = connect_to_dbase(omgeving)
        }
        date_risks = select_latest_risk_with_rmnodeid(query_rm, db_conn, max_date = all_dates[idx_d], extra_quant_cols='time,riskscore,chargeclass,timeclass,chanceclass', all_cols=T, close_connection = F)
        if (nrow(date_risks)>0){
          date_risks$pijldatum = all_dates[idx_d]
          date_risks = date_risks[,which(names(date_risks)!='accountingmethod.1'), with=F]
          if (nrow(stored_totalrisk)==0){
            stored_totalrisk = data.frame(date_risks[F,])
          }
          stored_totalrisk[(nrow(stored_totalrisk)+1):((nrow(stored_totalrisk))+nrow(date_risks)),]= date_risks
        }
        print(Sys.time()-start)
        
      }
    }


    print(paste('Run:', run))
    try(file.remove(list.files('./data/', pattern=paste0('total_data_[[:digit:]]*.[[:digit:]]*_',names(omgeving),'.R'), full.names = T)))
    try(file.remove(outputfile))
    save(stored_totalrisk, file= outputfile)
    print(Sys.time()-start)
    run = run + 1
    if (max_runs==-1){
      Sys.sleep(60*60*1)
    }
  }
    
}
