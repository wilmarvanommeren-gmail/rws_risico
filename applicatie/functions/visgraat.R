library(qcc)


cause.and.effect(cause=list(Measurements=list(Micrometers=list(1,2,3), Microscopes=list(1,2,3), Inspectors=list(1,2,3)),
                            Materials=c("Alloys", "Lubricants", "Suppliers"),
                            Personnel=c("Shifts", "Supervisors", "Training", "Operators"),
                            Environment=c("Condensation", "Moisture"),
                            Methods=c("Brake", "Engager", "Angle"),
                            Machines=c("Speed", "Lathes", "Bits", "Sockets")),
                 effect="Surface Flaws")

library(SixSigma)
# Cause and Effect Diagram
##Create effect as string
effect <- "Low Quality product"
##Create vector of causes
causes.gr <- c("Measurement", "Material", "Methods", "Environment",
               "Manpower", "Machines")
# Create indiviual cause as vector of list
causes <- vector(mode = "list", length = length(causes.gr))
causes[1] <- list(c("cmmon wilmar", "yess"))
causes[2] <- list(c("Raw Material", "Additive"))
causes[3] <- list(c("Sampling", "Analytical Procedure"))
causes[4] <- list(c("Rust near sample point"))
causes[5] <- list(c("Poor analyst","No guidance"))
causes[6] <- list(c("Leakage", "breakdown"))
# Create Cause and Effect Diagram
ss.ceDiag(effect, causes.gr, causes, sub = "Fish Bone Diagram Example")


mean2 <- function(x) {
  m <- rep(NA, length(x) - 1)
  for (i in 1:(length(x) - 1)) m[i] <- mean(x[c(i, i + 
                                                  1)])
  return(m)
}

cause=list(Oorzaak1=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak'), `sub-oorzaak2`=c("sub-oorzaak")),
           Oorzaak2=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak'), `sub-oorzaak2`=c("sub-oorzaak"), `sub-oorzaak3`=c('subsub-oorzaak','subsub-oorzaak',"sub-oorzaak")),
           Oorzaak3=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak'), `sub-oorzaak2`=c("sub-oorzaak"), `sub-oorzaak3`=c('subsub-oorzaak','subsub-oorzaak',"sub-oorzaak"), `sub-oorzaak4`=c("sub-oorzaak")),
           Oorzaak4=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak'), `sub-oorzaak2`=c("sub-oorzaak")),
           Oorzaak5=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak'), `sub-oorzaak2`=c("sub-oorzaak"), `sub-oorzaak3`=c('subsub-oorzaak','subsub-oorzaak',"sub-oorzaak")),
           Oorzaak6=list(`sub-oorzaak1`=c('subsub-oorzaak','subsub-oorzaak')))

effect="Effect"
# Get number of causes
title='test'
cex = c(1, 0.9, 1)
font = c(1, 3, 2)
n_causes <- length(cause)
n_causes_axis_upper <- n_causes - round(n_causes/2)
n_causes_axis_lower <- n_causes - n_causes_axis_upper
max_n_cause_items <- max(sapply(cause, length))
oldpar <- par(no.readonly = TRUE)
on.exit(par(oldpar))
par(bg = qcc.options("bg.margin"), cex = oldpar$cex * qcc.options("cex"), 
    mar = c(2, 2, 3, 2))

# Create plot area
plot(0:100, 0:100, type = "n", xlab = "", ylab = "", axes = FALSE)
rect(par("usr")[1], par("usr")[3], par("usr")[2], par("usr")[4], 
     col = qcc.options("bg.figure"))

# Add title
mtext(title, side = 3, line = par("mar")[3]/3, font = par("font.main"), 
      cex = qcc.options("cex"), col = par("col.main"))

#Add central arrow with effect text
strwidth_factor=.5
usr <- par("usr")
we <- strwidth(effect, units = "user") * strwidth_factor
wc <- max(unlist(sapply(cause, strwidth, units = "user")))
hc <- max(strheight(effect, units = "user"), unlist(sapply(cause, 
                                                           strheight, units = "user")))
main_arrow_end=usr[2] - we - 1
arrows(0, 50, main_arrow_end, 50, code = 2, length = 0.1, 
       angle = 20)
text(usr[2] - we, 50, effect, adj = c(0, 0.5), cex = cex[3], 
     font = font[3])

#Add causes axis to graph
padding =  usr[2]/100
axis_width <- (usr[2] - we)/(max(n_causes_axis_upper, n_causes_axis_lower)+.5)
axis_x_coordinate <- axis_width * (0:(max(n_causes_axis_upper, n_causes_axis_lower)))
axis_x_coordinate_diff = main_arrow_end-max(axis_x_coordinate)
axis_x_coordinate=axis_x_coordinate+axis_x_coordinate_diff-padding*3
for (i in 1:(length(axis_x_coordinate) - 1)) {
  segments(mean2(axis_x_coordinate)[i], 95, axis_x_coordinate[i + 1], 50)
  text(mean2(axis_x_coordinate)[i], 96, names(cause)[i], pos = 3, offset = 0.5, 
       cex = cex[1], font = font[3])
  if (i <= n_causes_axis_lower) {
    segments(mean2(axis_x_coordinate)[i], 5, axis_x_coordinate[i + 1], 50)
    text(mean2(axis_x_coordinate)[i], 4, names(cause)[[n_causes_axis_upper + i]], 
         pos = 1, offset = 0.5, cex = cex[1], font = font[3])
  }
}

# Add upper subcauses
for (j in 1:n_causes_axis_upper) {
  b <- (50 - 95)/(axis_x_coordinate[j + 1] - mean2(axis_x_coordinate)[j])
  a <- 95 - b * mean2(axis_x_coordinate)[j]
  y <- rev(50 + cumsum((95 - 50)/(max_n_cause_items + 1)) * (1:(max_n_cause_items)))
  x <- (y - a)/b
  for (i in 1:length(y)) {
    label <- names(cause[[j]][i])
    sub_cause_width <- strwidth(label, units = "user")*strwidth_factor
    subsub_causes = cause[[j]][i][[1]]
    if (!is.na(label)){
      arrows(x[i]-axis_width+sub_cause_width+padding, y[i], x[i], y[i], code = 2, length = 0.1, angle = 20)
      text(x[i]-axis_width+padding, y[i], label, pos = 4, offset = 0.2, 
           cex = cex[1], font = font[1])
      
      # Draw subcauses
      
      upper_sub_causes = round(length(subsub_causes)/2)
      lower_sub_causes = length(subsub_causes)-upper_sub_causes
      # Define width between segments
      sub_axis_width <- x[i]-(x[i]-axis_width+sub_cause_width+padding)*1.1
      sub_axis_x_coordinate <- sub_axis_width * (0:(max(upper_sub_causes, lower_sub_causes)))
      sub_axis_x_coordinate_diff = x[i]-max(sub_axis_x_coordinate)
      sub_axis_x_coordinate=sub_axis_x_coordinate+sub_axis_x_coordinate_diff-padding
      #

      for (h in 1:(length(sub_axis_x_coordinate) - 1)) {
        segments(mean2(sub_axis_x_coordinate)[h], y[i]+hc*.5, sub_axis_x_coordinate[h + 1], y[i])
        text(mean2(sub_axis_x_coordinate)[h]-strwidth(subsub_causes[h])*strwidth_factor*.25, y[i]+hc*.3, subsub_causes[h], pos = 3, offset = 0.5, 
             cex = cex[2], font = font[2])
        if (h <= lower_sub_causes) {
          segments(mean2(sub_axis_x_coordinate)[h], y[i]-hc*.5, sub_axis_x_coordinate[h + 1], y[i])
          text(mean2(sub_axis_x_coordinate)[h]-strwidth(subsub_causes[h])*strwidth_factor*.25, y[i]-hc*.3, subsub_causes[h], 
               pos = 1, offset = 0.5, cex = cex[2], font = font[2])
        }
      }
    }
  }
}

# Add lower subcauses
for (j in 1:n_causes_axis_upper) {
  b <- (50 - 5)/(axis_x_coordinate[j + 1] - mean2(axis_x_coordinate)[j])
  a <- 5 - b * mean2(axis_x_coordinate)[j]
  y <- cumsum((95 - 50)/(max_n_cause_items + 1)) * (1:(max_n_cause_items))
  x <- (y - a)/b
  if (j <= n_causes_axis_lower) 
    for (i in 1:length(y)) {
      label <- names(cause[[n_causes_axis_upper + j]][i])
      sub_cause_width <- strwidth(label, units = "user")*strwidth_factor
      if (!is.na(label)) 
        arrows(x[i]-axis_width+sub_cause_width+padding, y[i], x[i], y[i], code = 2, length = 0.1, angle = 20)
        text(x[i]-axis_width+padding, y[i], label, pos = 4, offset = 0.2, 
             cex = cex[1], font = font[1])
    }
}



invisible()