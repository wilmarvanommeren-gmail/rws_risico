
.ss.prepCanvas <-function (main = "Six Sigma graph", sub = "My Six Sigma Project", 
                           ss.col = c("#666666", "#BBBBBB", "#CCCCCC", "#DDDDDD", "#EEEEEE"), width=1, height=1, font_factor=1) 
{
  grid::grid.newpage()
  grid::grid.rect(gp = grid::gpar(col = ss.col[4], lwd = 2, 
                                  fill = ss.col[1]))
  vp.canvas <- grid::viewport(name = "canvas", 
                              width = unit(width,  "npc") - unit(6, "mm"), 
                              height = unit(height, "npc") - unit(6,  "mm"), 
                              layout = grid::grid.layout(3, 1, heights = unit(c(3, 1, 2), c("lines", "null", "lines"))))
  grid::pushViewport(vp.canvas)
  grid::grid.rect(gp = grid::gpar(col = "#FFFFFF", lwd = 0, 
                                  fill = "#FFFFFF"))
  vp.title <- grid::viewport(layout.pos.col = 1, layout.pos.row = 1, 
                             name = "title")
  grid::pushViewport(vp.title)
  grid::grid.text(main, gp = grid::gpar(fontsize = 15*font_factor))
  grid::popViewport()
  vp.subtitle <- grid::viewport(layout.pos.col = 1, layout.pos.row = 3, 
                                name = "subtitle")
  grid::pushViewport(vp.subtitle)
  grid::grid.text(sub, gp = grid::gpar(col = ss.col[1]))
  grid::popViewport()
  vp.container <- grid::viewport(layout.pos.col = 1, layout.pos.row = 2, 
                                 name = "container")
  grid::pushViewport(vp.container)
}

ce_diagram <-function (effect, causes.gr, causes, main = "Six Sigma Cause-and-effect Diagram", 
                       sub, ss.col = c("#666666", "#BBBBBB", "#CCCCCC", "#DDDDDD", 
                                       "#EEEEEE"), effect_color=NULL, causes.gr_color=NULL,causes_color=NULL) 
{
  n.causes <- length(causes.gr)
  
  size_factor = .5+n.causes*.075
  
  font_factor =1# 5/n.causes
  x_offset = n.causes*.0015
  
  # Create plot canvas
  .ss.prepCanvas(main, sub, ss.col, font_factor=font_factor)
  
  # Plot effect
  effect_wrap=100
  causes.gr_wrap = 80
  causes_wrap = 100
  effect_wrapped = strwrap(effect, effect_wrap)
  effect_string = paste(effect_wrapped , collapse = '\n')
  w.head <- unit(1, "strwidth", effect_string) + unit(1, "mm")
  
  vp.head <- grid::viewport(x = unit(1, "npc") - w.head, height = unit(1, "strheight", effect_string) + unit(2, "mm"), 
                            width = w.head, just = c("left", "center"))
  grid::pushViewport(vp.head)
  x.hshape <- c(0, 0, .98, .98)
  y.hshape <- c(0, 1, 1, 0)
  grid::grid.xspline(x.hshape, y.hshape, shape = c(0, 0, 0, 0), open = FALSE, gp = grid::gpar(col = ss.col[2], 
                                                                                              lwd = 2, fill = ss.col[5]))
  grid::grid.text(effect_string, 
                  gp = grid::gpar(fontsize = 10*font_factor))
  
  
  grid::popViewport()

  # Plot tail of fishbone
  vp.tail <- grid::viewport(x = 0.01, height = 1, width = 0.05, 
                            just = c("left", "center"))
  grid::pushViewport(vp.tail)
  grid::grid.xspline(x = c(0, 0, 0.4, 0, 0, 1), y = c(0.44, 0.48, 0.5, 0.52, 0.56, 0.5), 
                     shape = c(0, 0, 0, 0, 0, 0), open = FALSE, gp = grid::gpar(col = ss.col[2], 
                                                                                lwd = 2, fill = ss.col[5]))
  grid::popViewport()
  
  # plot line between tail and head
  vp.body <- grid::viewport(x = 0.06, height = 1, width = unit(1, 
                                                               "npc") - w.head - unit(0.06, "npc"), just = c("left", 
                                                                                                             "center"))
  grid::pushViewport(vp.body)
  grid::grid.lines(x = c(0, 1), y = c(0.5, 0.5), gp = grid::gpar(col = ss.col[2], 
                                                                 lwd = 4))
  # add color to effect
  if (!is.null(effect_color)){
    grid::grid.circle(x= .99, r=.024
                      , gp=gpar(fill=effect_color, col=effect_color))
  }
  

  
  m <- (n.causes%/%2) + 1
  if (m>1){
    pUp <- seq(1/m, 1 - (1/m), 1/m)
  } else {
    pUp = c(.5)
  }
  if (n.causes>0){
    # check if there are causes filled
    no_causes = all(unlist(lapply(causes, function(x)is.null(x))))
    y_height = .9
    if (no_causes){
      y_height = .7
    }
    
    pUp.width = list()
    pUp = pUp +x_offset

    angle = .05
    for (i in 1:(length(pUp))) {
      grid::grid.lines(x = c(pUp[i] - angle, pUp[i]), y = c(y_height, 0.5), gp = grid::gpar(col = ss.col[2], lwd = 2))
      grid::grid.text(paste0(strwrap(causes.gr[i], causes.gr_wrap), collapse = '\n'), x = pUp[i] - angle, y = y_height+0.01, 
                      just = c("center", "bottom"),check.overlap=T, 
                      gp = grid::gpar(fontsize = 10*font_factor))
      if (!is.null(causes.gr_color)){
        if (length(causes.gr_color)==length(causes.gr)){
          grid::grid.circle(x = pUp[i] - angle, y = y_height-.01, r=.012
                            , gp=gpar(fill=causes.gr_color[i], col=causes.gr_color[i]))
        }
      }
      
      causes_y_coords = seq(.5, y_height, length.out  = length(causes[[i]])+2)[2:(length(causes[[i]])+1)]
      causes_x_coords =  rev(seq(pUp[i] - angle,pUp[i], length.out  = length(causes[[i]])+2)[2:(length(causes[[i]])+1)])+.01
      if (length(causes[[i]])>0){
        for (j in 1:length(causes[[i]])) {
          grid::grid.text(paste0(strwrap(causes[[i]][j], causes_wrap), collapse = '\n'), x = causes_x_coords[j], y = unit(causes_y_coords[j], "npc"), 
                          gp = grid::gpar(fontsize = 8*font_factor), 
                          just = c("left", "center"),check.overlap=T)
          if (!is.null(causes_color)){
            if (length(causes_color)==length(causes) & length(causes_color[[i]])==length(causes[[i]])){
              grid::grid.circle(x = causes_x_coords[j]-.01, y = unit(causes_y_coords[j], "npc"), r=.006
                                , gp=gpar(fill=causes_color[[i]][j], col=causes_color[[i]][j]))}}
        }
      }
    }
    if (n.causes>1){
      y_height = .1
      if (no_causes){
        y_height = .3
      }
      
      pDown <- pUp + (1/(2 * m))
      if (n.causes%%2 != 0) 
        pDown <- c(1/(m * 2)+x_offset, pDown)
      k <- length(pDown)
      causes.gr_wrap = 250/(length(pUp) + 1)*size_factor
      causes_wrap = 300/(length(pUp) + 1)*size_factor
      for (i in (length(pUp) + 1):n.causes) {
        grid::grid.lines(x = c((pDown[k] - angle), pDown[k]), 
                         y = c(y_height, 0.5), gp = grid::gpar(col = ss.col[2], 
                                                          lwd = 2))
        grid::grid.text(paste0(strwrap(causes.gr[i], causes.gr_wrap), collapse = '\n'), x = pDown[k] -angle, y = y_height-.01, 
                        just = c("center", "top"),check.overlap=T, 
                        gp = grid::gpar(fontsize = 10*font_factor))
        if (!is.null(causes.gr_color)){
          if (length(causes.gr_color)==length(causes.gr)){
            grid::grid.circle(x = pDown[k] - angle, y = y_height+.01, r=.012
                              , gp=gpar(fill=causes.gr_color[i], col=causes.gr_color[i]))
          }
        }
        
        causes_y_coords = seq(y_height, 0.5, length.out  = length(causes[[i]])+2)[2:(length(causes[[i]])+1)]
        causes_x_coords =  seq(pDown[k] - angle,  pDown[k], length.out  = length(causes[[i]])+2)[2:(length(causes[[i]])+1)]+.006
        if (length(causes[[i]])>0){
          for (j in 1:length(causes[[i]])) {
            grid::grid.text(paste0(strwrap(causes[[i]][j], causes_wrap), collapse = '\n'), x = causes_x_coords[j], y = unit(causes_y_coords[j],"npc") , 
                            gp = grid::gpar(fontsize = 8*font_factor), 
                            just = c("left", "center"),check.overlap=T)
            if (!is.null(causes_color)){
              if (length(causes_color)==length(causes) & length(causes_color[[i]])==length(causes[[i]])){
                grid::grid.circle(x = causes_x_coords[j]-.01, y = unit(causes_y_coords[j], "npc"), r=.01
                                  , gp=gpar(fill=causes_color[[i]][j], col=causes_color[[i]][j]))}}
          }
        }
        k <- (k - 1)
      }
    }
  }
  
  
}